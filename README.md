# Sample app for testing rendering d3 component in react and sharing state.

This is one way I've found to share state between d3 and react without rerendering the whole chart each time state changes, however the implementation is not ideal because we check for presence of the svg element with `if(svg.nodes()[0].children.length === 0)`.
